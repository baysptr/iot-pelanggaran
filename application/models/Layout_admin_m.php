<?php
	class Layout_admin_m extends CI_Model{
		public function meta(){
			$html = '<title>IOT PELANGGARAN | ADMIN</title>
				<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
				<link href="'.base_url().'bower_components/materialize/dist/css/materialize.min.css" rel="stylesheet">';
			return $html;
		}
		
		public function sidebar(){
			$html = '<ul id="dropdown1" class="dropdown-content">
						<li><a href="#!">Data Pengguna</a></li>
						<li class="divider"></li>
						<li><a href="#!">Data Kendaraan</a></li>
					</ul>
					<ul id="dropdown2" class="dropdown-content">
						<li><a href="#!">Data Pengguna</a></li>
						<li class="divider"></li>
						<li><a href="#!">Data Kendaraan</a></li>
					</ul>
					<nav>
						<div class="nav-wrapper">
							<a href="#!" class="brand-logo"><img src="'.base_url().'bower_components/img/logo_fasilkom.png" width="30%"></a>
							<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">keyboard_return</i></a>
							<ul class="right hide-on-med-and-down">
								<li><a class="waves-effect waves-light btn">Data Pelanggaran <i class="material-icons right">do_not_disturb_on</i></a></li>
								<li><a class="waves-effect waves-light btn" href="'.site_url().'/admin/pengguna">Data Pengguna <i class="material-icons right">supervisor_account</i></a></li>
								<li><a class="dropdown-trigger waves-effect waves-light btn" href="#!" data-target="dropdown1">Temporary Data<i class="material-icons right">delete_sweep</i></a></li>
								<li><a class="waves-effect waves-light btn" href="javascript:;" onclick="if(confirm(\'Apakah anda yakin akan keluar?\')===true){ window.location=\''.site_url().'/welcome/do_logout\' }">Logout <i class="material-icons right">lock_open</i></a></li>
							</ul>
						</div>
					</nav>
					<ul class="sidenav" id="mobile-demo">
						<li><a class="waves-effect waves-light btn">Data Pelanggaran <i class="material-icons right">do_not_disturb_on</i></a></li>
						<li><a class="waves-effect waves-light btn" href="'.site_url().'/admin/pengguna">Data Pengguna <i class="material-icons right">supervisor_account</i></a></li>
						<li><a class="dropdown-trigger waves-effect waves-light btn" href="#!" data-target="dropdown2">Temporary Data<i class="material-icons right">delete_sweep</i></a></li>
						<li><a class="waves-effect waves-light btn" href="javascript:;" onclick="if(confirm(\'Apakah anda yakin akan keluar?\')===true){ window.location=\''.site_url().'/welcome/do_logout\' }">Logout <i class="material-icons right">lock_open</i></a></li>
					</ul>';
			return $html;
		}
		
		public function script(){
			$html = '<script src="'.base_url().'bower_components/jquery/dist/jquery.min.js"></script>
					<script src="'.base_url().'bower_components/materialize/dist/js/materialize.min.js"></script>
					<script>
						$(document).ready(function(){
							$(".sidenav").sidenav();
							$(".dropdown-trigger").dropdown();
						});
					</script>';
			return $html;
		}
	}
