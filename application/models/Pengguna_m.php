<?php
	class Pengguna_m extends CI_Model{
		public function simpan($data){
			return $this->db->insert('pengguna', $data);
		}

		public function check_login($data){
			return $this->db->get_where('pengguna', $data);
		}

		public function getAll(){
			return $this->db->get_where('pengguna', array("status" => 'on'))->result_array();
		}

		public function getWhere($id){
			return $this->db->get_where('pengguna', array("id" => $id))->row();
		}

		public function update($data, $id){
			return $this->db->update('pengguna', $data, array("id" => $id));
		}
	}
