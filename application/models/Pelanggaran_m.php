<?php
/**
 * Created by PhpStorm.
 * User: undefined-PC
 * Date: 24/12/2018
 * Time: 16:58
 */

class Pelanggaran_m extends CI_Model{

	public function getCheck($id_kendaraan, $jenis, $tgl, $latlong){
		return $this->db->get_where('pelanggaran', array(
			'lat_long' => $latlong,
			'id_kendaraan' => $id_kendaraan,
			'jenis_pelanggaran' => $jenis,
			'tgl_post' => $tgl
		))->num_rows();
	}

	public function simpan($data){
		return $this->db->insert('pelanggaran', $data);
	}

	public function getWhere($id_pengguna){
		return $this->db
					->select("kendaraan.id_kendaraan, 
					kendaraan.kode_kendaraan, 
					kendaraan.jenis_kendaraan, 
					kendaraan.merk_kendaraan, 
					kendaraan.plat_nomor, 
					pelanggaran.jenis_pelanggaran, 
					pelanggaran.kode_pelanggaran, 
					pelanggaran.tgl_post, 
					pelanggaran.lat_long")
					->from("kendaraan")
					->join("pelanggaran", "pelanggaran.id_kendaraan = kendaraan.id_kendaraan")
					->where("kendaraan.id_pengguna", $id_pengguna)
					->get();
	}

}
