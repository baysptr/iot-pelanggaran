<?php
/**
 * Created by PhpStorm.
 * User: undefined-PC
 * Date: 23/12/2018
 * Time: 20:09
 */

class Kendaraan_m extends CI_Model{
	public function simpan($data){
		return $this->db->insert('kendaraan', $data);
	}

	public function getALl(){
		return $this->db->get_where('kendaraan', array("status" => 'on'));
	}

	public function getWhere($id){
		return $this->db->get_where('kendaraan', array("id_kendaraan" => $id))->row();
	}

	public function moveTrash($id){
		return $this->db->update('kendaraan', array("status" => "off"), array("id_kendaraan" => $id));
	}

	public function update($data, $id){
		return $this->db->update('kendaraan', $data, array("id_kendaraan" => $id));
	}

	public function whereKode($kode){
		return $this->db->get_where('kendaraan', array('kode_kendaraan' => $kode))->row();
	}
}
