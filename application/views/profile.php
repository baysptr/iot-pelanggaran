<html>
<head>
	<?= $meta ?>
</head>
<body>
<?= $sidebar ?>

<div class="container"><br/>
	<div class="row">
		<form class="col s12" action="<?= site_url() ?>/pengguna/update_pengguna" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="input-field col s12 m6">
					<i class="material-icons prefix">account_circle</i>
					<input id="nama" type="text" name="nama" autofocus value="<?= $pengguna->nama ?>">
					<label for="nama">Nama Lengkap</label>
				</div>
				<div class="input-field col s12 m6">
					<i class="material-icons prefix">assignment_ind</i>
					<input id="nik" type="text" name="nik" value="<?= $pengguna->nik ?>">
					<label for="nik">NIK</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m6">
					<i class="material-icons prefix">add_location</i>
					<input id="alamat" type="text" name="alamat" value="<?= $pengguna->alamat ?>">
					<label for="Alamat">Alamat</label>
				</div>
				<div class="input-field col s12 m6">
					<select id="jk" name="jk">
						<option disabled>-- Pilih jenis kelamin --</option>
						<option value="Laki - laki" <?= ($pengguna->jenis_kelamin == 'Laki - laki') ? 'selected' : '' ?>>Laki - laki</option>
						<option value="Perempuan" <?= ($pengguna->jenis_kelamin == 'Perempuan') ? 'selected' : '' ?>>Perempuan</option>
					</select>
					<label>Jenis Kelamin</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m6">
					<i class="material-icons prefix">contact_mail</i>
					<input id="email" type="email" name="email" value="<?= $pengguna->email ?>">
					<label for="email">e-Mail</label>
				</div>
				<div class="input-field col s12 m6">
					<i class="material-icons prefix">contact_phone</i>
					<input id="telp" type="text" name="telp" value="<?= $pengguna->no_telp ?>">
					<label for="telp">No. Telp</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m6">
					<i class="material-icons prefix">fingerprint</i>
					<input id="pass" type="password" name="pass">
					<label for="pass">Password baru</label>
				</div>
				<div class="file-field input-field col s12 m6">
					<div class="btn">
						<span>File</span>
						<input type="file" id="file_foto" name="file_foto">
					</div>
					<div class="file-path-wrapper">
						<input class="file-path validate" type="text" placeholder="Upload Foto Profile">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m6">
					<label>
						<input type="checkbox" name="c_password"/>
						<span>* centang untuk ubah password</span>
					</label>
					<button type="submit" class="waves-effect waves-light btn"><i class="material-icons right">assignment_turned_in</i> Daftar</button>
				</div>
				<div class="col s12 m6">
					<label>
						<input type="checkbox" name="c_foto"/>
						<span>* centang untuk ubah foto profile</span>
					</label>
					<img class="materialboxed" width="150" src="<?= base_url() ?>resource/pengguna/<?= $pengguna->file_foto ?>" id="preview-foto">
				</div>
			</div>
		</form>
	</div>
</div>

<?= $script ?>

<script>
	$(document).ready(function(){
		$('select').formSelect();
		$('.materialboxed').materialbox();
		$('.sidenav').sidenav();

		function previewPhotoProfile(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#preview-foto').attr('src', e.target.result);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}

		$("#file_foto").change(function () {
			previewPhotoProfile(this);
		});
	});
</script>
</body>
</html>
