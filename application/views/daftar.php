<html>
	<head>
		<title>IOT PELANGGARAN | WEB</title>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="<?= base_url() ?>bower_components/materialize/dist/css/materialize.min.css" rel="stylesheet">
	</head>
	<body>
		<nav>
			<div class="nav-wrapper">
				<a href="#!" class="brand-logo center"><img src="<?= base_url() ?>bower_components/img/logo_fasilkom.png" width="30%"></a>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul class="right hide-on-med-and-down">
					<li><a href="<?= site_url() ?>/welcome/login" class="waves-effect waves-light btn"><i class="material-icons right">keyboard_return</i> Sudah punya akun!</a></li>
				</ul>
			</div>
		</nav>

		<ul class="sidenav" id="mobile-demo">
			<li><a href="<?= site_url() ?>/welcome/login" class="waves-effect waves-light btn"><i class="material-icons right">keyboard_return</i> Sudah punya akun!</a></li>
		</ul>
		<div class="container">
			<div class="row">
				<div class="col s12">
					<h3 class="center">
						Form Daftar Pengguna
					</h3>
				</div>
				<form class="col s12" action="<?= site_url() ?>/welcome/do_daftar" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">account_circle</i>
							<input id="nama" type="text" name="nama" autofocus>
							<label for="nama">Nama Lengkap</label>
						</div>
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">assignment_ind</i>
							<input id="nik" type="text" name="nik">
							<label for="nik">NIK</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">add_location</i>
							<input id="alamat" type="text" name="alamat">
							<label for="Alamat">Alamat</label>
						</div>
						<div class="input-field col s12 m6">
							<select id="jk" name="jk">
								<option disabled selected>-- Pilih jenis kelamin --</option>
								<option value="Laki - laki">Laki - laki</option>
								<option value="Perempuan">Perempuan</option>
							</select>
							<label>Jenis Kelamin</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">contact_mail</i>
							<input id="email" type="email" name="email">
							<label for="email">e-Mail</label>
						</div>
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">contact_phone</i>
							<input id="telp" type="text" name="telp">
							<label for="telp">No. Telp</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m6">
							<i class="material-icons prefix">fingerprint</i>
							<input id="pass" type="password" name="pass">
							<label for="pass">Password</label>
						</div>
						<div class="file-field input-field col s12 m6">
							<div class="btn">
								<span>File</span>
								<input type="file" id="file_foto" name="file_foto">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text" placeholder="Upload Foto Profile">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m6">
							<button type="submit" class="waves-effect waves-light red btn"><i class="material-icons right">cancel</i> Batal</button>
							<button type="submit" class="waves-effect waves-light btn"><i class="material-icons right">assignment_turned_in</i> Daftar</button>
						</div>
						<div class="col s12 m6">
							<img class="materialboxed" width="150" src="https://us.123rf.com/450wm/martialred/martialred1507/martialred150700529/42561784-a%C3%B1adir-nuevo-icono-plana-cuenta-de-usuario-para-aplicaciones-y-sitios-web.jpg?ver=6" id="preview-foto">
						</div>
					</div>
				</form>
			</div>
		</div>

		<script src="<?= base_url() ?>bower_components/jquery/dist/jquery.min.js"></script>
		<script src="<?= base_url() ?>bower_components/materialize/dist/js/materialize.min.js"></script>
		<script>
			$(document).ready(function(){
				$('select').formSelect();
				$('.materialboxed').materialbox();
				$('.sidenav').sidenav();

				function previewPhotoProfile(input) {
					if (input.files && input.files[0]) {
						var reader = new FileReader();

						reader.onload = function (e) {
							$('#preview-foto').attr('src', e.target.result);
						};
						reader.readAsDataURL(input.files[0]);
					}
				}

				$("#file_foto").change(function () {
					previewPhotoProfile(this);
				});
			});
		</script>
	</body>
</html>
