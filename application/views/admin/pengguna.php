<html>
<head>
	<?= $meta ?>
</head>
<body>
<?= $sidebar ?>

<div class="container">
	<div class="row">
		<div class="col s12">
			<h3 class="center">
				Pengguna Page
			</h3>
			<table class="highlight responsive-table">
				<thead>
					<tr>
						<td>NAMA</td>
						<td>JENIS KELAMIN</td>
						<td>EMAIL</td>
						<td>NO. TELP</td>
						<td colspan="2" class="center">Aksi</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($pengguna as $data){ ?>
						<?php if($data['credentials'] != "admin"){ ?>
							<tr>
								<td><?= $data['nama'] ?></td>
								<td><?= $data['jenis_kelamin'] ?></td>
								<td><?= $data['email'] ?></td>
								<td><?= $data['no_telp'] ?></td>
								<td><div class="waves-effect waves-light red btn"><i class="material-icons">delete</i></div></td>
								<td><div class="waves-effect waves-light btn"><i class="material-icons">remove_red_eye</i></div></td>
							</tr>
					<?php } } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?= $script ?>
</body>
</html>
