<html>
	<head>
		<title>IOT PELANGGARAN | WEB</title>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="<?= base_url() ?>bower_components/materialize/dist/css/materialize.min.css" rel="stylesheet">
	</head>
	<body>
		<nav>
			<div class="nav-wrapper">
				<a href="#!" class="brand-logo center"><img src="<?= base_url() ?>bower_components/img/logo_fasilkom.png" width="30%"></a>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul class="right hide-on-med-and-down">
					<li><a href="<?= site_url() ?>/welcome/daftar" class="waves-effect waves-light btn"><i class="material-icons right">keyboard_tab</i> Belum punya akun!</a></li>
				</ul>
			</div>
		</nav>

		<ul class="sidenav" id="mobile-demo">
			<li><a href="<?= site_url() ?>/welcome/daftar" class="waves-effect waves-light btn"><i class="material-icons right">keyboard_tab</i> Belum punya akun!</a></li>
		</ul>
		<div class="container">
			<div class="row">
				<div class="col s12">
					<h3 class="center">
						Login Pengguna
					</h3>
				</div>
				<form class="col s12" action="<?= site_url() ?>/welcome/do_login" method="post">
					<div class="row">
						<div class="input-field col s12 m12">
							<i class="material-icons prefix">account_box</i>
							<input id="email" type="email" name="email" autofocus>
							<label for="email">e-Mail</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12 m12">
							<i class="material-icons prefix">assignment_late</i>
							<input id="pass" type="password" name="password">
							<label for="pass">Password</label>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m12 center">
							<button type="submit" class="waves-effect waves-light btn z-depth-3"><i class="material-icons right">lock_open</i> Masuk!</button>
						</div>
					</div>
				</form>
			</div>
		</div>

		<script src="<?= base_url() ?>bower_components/jquery/dist/jquery.min.js"></script>
		<script src="<?= base_url() ?>bower_components/materialize/dist/js/materialize.min.js"></script>
		<script>
			$(document).ready(function(){
				$('select').formSelect();
				$('.materialboxed').materialbox();
				$('.sidenav').sidenav();
			});
		</script>
	</body>
</html>
