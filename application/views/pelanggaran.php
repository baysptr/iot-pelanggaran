<html>
<head>
	<?= $meta ?>
</head>
<body>
<?= $sidebar ?>

<div class="container">
	<div class="row">
		<div class="col s12">
			<h3 class="center">
				Pelanggaran Page
			</h3>
			<table class="highlight">
				<thead class="grey white-text">
					<tr>
						<td>MERK KENDARAAN</td>
						<td>JENIS KENDARAAN</td>
						<td>PLAT NOMOR</td>
						<td>JENIS PELANGGARAN</td>
						<td colspan="3">&nbsp;</td>
					</tr>
				</thead>
				<tbody>
					<?php if($pelanggaran->num_rows() <= 0){ ?>
						<tr><td colspan="5"><strong>DATA PELANGGARAN ANDA TIDAK ADA</strong></td></tr>
					<?php }else{ foreach ($pelanggaran->result_array() as $data){ ?>
						<tr>
							<td><?= $data['merk_kendaraan'] ?></td>
							<td><?= $data['jenis_kendaraan'] ?></td>
							<td><?= $data['plat_nomor'] ?></td>
							<td><?= $data['jenis_pelanggaran'] ?></td>
							<td><div onclick="detailPelanggar()" class="waves-effect waves-light orange btn tooltipped" data-position="top" data-tooltip="Detail Pelanggaran"><i class="material-icons">remove_red_eye</i> </div> </td>
							<td><div onclick="detailMap('<?= $data['lat_long'] ?>')" class="waves-effect waves-light blue btn tooltipped" data-position="left" data-tooltip="Lihat Lokasi Pelanggaran"><i class="material-icons">place</i> </div> </td>
							<td><div onclick="detailQrcode('<?= urlencode(site_url('pelanggaran/scan/').$data['kode_pelanggaran']) ?>')" class="waves-effect waves-light red btn tooltipped" data-position="right" data-tooltip="Scan Pelanggaran"><i class="material-icons">phonelink_ring</i> </div> </td>
						</tr>
					<?php } } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!--Modal Kendaraan-->
<div id="modalView" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Detail Pelanggaran</h4>
		<table class="highlight">
			<tr>
				<td>Jenis Pelanggaran</td>
				<td id="vPelanggaran">test</td>
			</tr>
			<tr>
				<td>Merk Kendaraan</td>
				<td id="vMerk">test</td>
			</tr>
			<tr>
				<td>Jenis Kendaraan</td>
				<td id="vKendaraan">test</td>
			</tr>
			<tr>
				<td>Plat Nomor</td>
				<td id="vPlat">test</td>
			</tr>
			<tr>
				<td>Tanggal Pelanggan</td>
				<td id="vTanggal">test</td>
			</tr>
			<tr>
				<td>Kode Pelanggaran</td>
				<td id="vKodePel">test</td>
			</tr>
			<tr>
				<td>Kode Kendaraan</td>
				<td id="vKodeKen">test</td>
			</tr>
		</table>
	</div>
	<div class="modal-footer">
		<div onclick="closeView()" class="waves-effect waves-light orange btn"><i class="material-icons right">reply</i> Close</div>
	</div>
</div>

<div id="modalMap" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Lokasi Infrastruktur</h4>
		<iframe id="vMap" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.5668657391216!2d112.77463791403538!3d-7.290019794738331!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x61e1066f050ef37c!2sRodo+Pizza+%26+Bar!5e0!3m2!1sid!2sid!4v1545672783366" width="650" height="260" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	<div class="modal-footer">
		<div onclick="closeMap()" class="waves-effect waves-light orange btn"><i class="material-icons right">reply</i> Close</div>
	</div>
</div>

<div id="modalBarcode" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Scan QrCode</h4>
		<img id="vQrcode" src="https://api.qrserver.com/v1/create-qr-code/?data=HelloWorld&amp;size=350x250" alt="" title="" />
	</div>
	<div class="modal-footer">
		<div onclick="closeQrcode()" class="waves-effect waves-light orange btn"><i class="material-icons right">reply</i> Close</div>
	</div>
</div>

<?= $script ?>

<script>
	$(document).ready(function(){
		$('.modal').modal();
		$('.tooltipped').tooltip();
	});
	
	function closeView() {
		$("#modalView").modal('close');
	}

	function detailPelanggar() {
		$("#modalView").modal('open');
	}

	function closeMap() {
		$("#modalMap").modal('close');
	}

	function detailMap(latlong) {
		$("#vMap").attr("src", "https://maps.google.com/maps?q="+latlong+"&z=20&output=embed");
		$("#modalMap").modal('open');
	}

	function closeQrcode() {
		$("#modalBarcode").modal('close');
	}

	function detailQrcode(kode) {
		$("#vQrcode").attr("src", "http://chart.apis.google.com/chart?cht=qr&chs=300x300&chl="+kode+"&chld=H|0");
		$("#modalBarcode").modal('open');
	}
</script>
</body>
</html>
