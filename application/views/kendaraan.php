<html>
<head>
	<?= $meta ?>
</head>
<body>
<?= $sidebar ?>

<div class="container">
	<div class="row">
		<div class="col s12">
			<h3 class="center">
				Kendaraan Page
			</h3>
			<table class="highlight responsive-table">
				<thead>
					<tr>
						<td>JENIS KENDARAAN</td>
						<td>MERK KENDARAAN</td>
						<td>PLAT NOMOR KENDARAAN</td>
						<td colspan="3">
							<div onclick="addKendaraan()" class="waves-effect waves-light blue btn"><i class="material-icons">add_box</i></div>
						</td>
					</tr>
				</thead>
				<tbody>
					<?php if($kendaraan->num_rows() <= 0){ ?>
						<tr><td colspan="5"><center>DATA KENDARAAN ANDA BELUM TERSEDIA</center></td></tr>
					<?php }else{ foreach ($kendaraan->result_array() as $data){ ?>
						<tr>
							<td><?= $data['jenis_kendaraan'] ?></td>
							<td><?= $data['merk_kendaraan'] ?></td>
							<td><?= $data['plat_nomor'] ?></td>
							<td>
								<div onclick="hapusKendaraan('<?= $data['id_kendaraan'] ?>')" class="waves-effect waves-light red btn"><i class="material-icons">delete</i> </div>
							</td>
							<td>
								<div onclick="editModal('<?= $data['id_kendaraan'] ?>')" class="waves-effect waves-light btn"><i class="material-icons">edit</i> </div>
							</td>
							<td>
								<div onclick="showView('<?= $data['id_kendaraan'] ?>')" class="waves-effect waves-light deep-purple btn"><i class="material-icons">remove_red_eye</i> </div>
							</td>
						</tr>
					<?php } } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!--Modal Kendaraan-->
<div id="modal1" class="modal">
	<div class="modal-content">
		<h4>Form Tambah Kendaraan</h4>
		<form action="<?= site_url() ?>/pengguna/do_kendaraan" method="post" id="formKendaraan">
			<input type="hidden" name="id" id="id">
			<table class="striped">
				<tr>
					<td>JENIS KENDARAAN</td>
					<td>:</td>
					<td><input type="text" name="jenis" id="jenis"></td>
				</tr>
				<tr>
					<td>MERK KENDARAAN</td>
					<td>:</td>
					<td><input type="text" name="merk" id="merk"></td>
				</tr>
				<tr>
					<td>PLAT NOMOR</td>
					<td>:</td>
					<td><input type="text" name="plat" id="plat"></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
					<td>
						<button type="button" class="waves-effect waves-light red btn" onclick="closeKendaraan()">Batal</button>
						<button type="button" class="waves-effect waves-light btn" onclick="simpanKendaraan()">Simpan</button>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>

<!--Modal Kendaraan-->
<div id="modalView" class="modal">
	<div class="modal-content">
		<h4>Detail Kendaraan</h4>
		<table class="highlight">
			<tr>
				<td>KODE KENDARAAN</td>
				<td>:</td>
				<td id="vKode"></td>
			</tr>
			<tr>
				<td>JENIS KENDARAAN</td>
				<td>:</td>
				<td id="vJenis"></td>
			</tr>
			<tr>
				<td>MERK KENDARAAN</td>
				<td>:</td>
				<td id="vMerk"></td>
			</tr>
			<tr>
				<td>PLAT NOMOR</td>
				<td>:</td>
				<td id="vPlat"></td>
			</tr>
		</table>
	</div>
	<div class="modal-footer">
		<div onclick="closeView()" class="waves-effect waves-light orange btn"><i class="material-icons right">reply</i> Close</div>
	</div>
</div>

<?= $script ?>
<script>
	$(document).ready(function(){
		$('.modal').modal();
	});

	var method;

	function closeView() {
		$("#vKode").html("");
		$("#vJenis").html("");
		$("#vMerk").html("");
		$("#vPlat").html("");
		$("#modalView").modal('close');
	}

	function showView(id) {
		$.ajax({
			url: "<?php echo site_url('pengguna/detail_kendaraan') ?>/" + id,
			type: "GET",
			dataType: "JSON",
			success: function (data)
			{
				$("#vKode").html(data.kode_kendaraan);
				$("#vJenis").html(data.jenis_kendaraan);
				$("#vMerk").html(data.merk_kendaraan);
				$("#vPlat").html(data.plat_nomor);
				$("#modalView").modal('open');
			}
		});
	}

	function showView(id) {
		$.ajax({
			url: "<?php echo site_url('pengguna/detail_kendaraan') ?>/" + id,
			type: "GET",
			dataType: "JSON",
			success: function (data)
			{
				$("#vKode").html(data.kode_kendaraan);
				$("#vJenis").html(data.jenis_kendaraan);
				$("#vMerk").html(data.merk_kendaraan);
				$("#vPlat").html(data.plat_nomor);
				$("#modalView").modal('open');
			}
		});
	}

	function editModal(id) {
		$.ajax({
			url: "<?php echo site_url('pengguna/detail_kendaraan') ?>/" + id,
			type: "GET",
			dataType: "JSON",
			success: function (data)
			{
				method = "update";
				$("#id").val(data.id_kendaraan);
				$("#jenis").val(data.jenis_kendaraan);
				$("#merk").val(data.merk_kendaraan);
				$("#plat").val(data.plat_nomor);
				$("#modal1").modal('open');
			}
		});
	}

	function addKendaraan() {
		method = "tambah";
		$("#modal1").modal("open");
	}

	function closeKendaraan() {
		$("#formKendaraan")[0].reset();
		$("#modal1").modal("close");
	}

	function simpanKendaraan() {
		var url;
		if(method === "tambah"){
			url = "<?= site_url() ?>/pengguna/do_kendaraan";
		}else if(method === "update"){
			url = "<?= site_url() ?>/pengguna/update_kendaraan";
		}

		$.ajax({
			url: url,
			type: 'POST',
			processData: false,
			contentType: false,
			data: new FormData($("#formKendaraan")[0]),
			success: function (data) {
				if (data === 1) {
					alert("Data Gagal Input !!!");
				} else {
					window.location.reload();
				}
			}
		});
	}

	function hapusKendaraan(id) {
		if(confirm("apakah anda yakin untuk menghapus data ini?")===true){
			$.ajax({
				url: "<?php echo site_url('pengguna/delete_kendaraan') ?>/" + id,
				type: "GET",
				dataType: "JSON",
				success: function ()
				{
					window.location.reload();
				}
			});
		}
	}
</script>
</body>
</html>
