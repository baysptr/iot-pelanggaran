<?php
date_default_timezone_set('Asia/Jakarta');
class Param extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kendaraan_m');
		$this->load->model('Pelanggaran_m');
	}

	public function iot(){
		$jenis = $this->input->post('jenis');
		$langlot = $this->input->post('langlot');
		$kode = $this->input->post('kode');
		$tgl = $this->input->post('tgl');
		$dataKendaraan = $this->Kendaraan_m->whereKode($kode);
		$kodePelanggaran = "PELANGGAR-".uniqid();

		$check = $this->Pelanggaran_m->getCheck($dataKendaraan->id_kendaraan, $jenis, $tgl, $langlot);
		if($check <= 0){
			$data = [
				"id_kendaraan" => $dataKendaraan->id_kendaraan,
				"kode_pelanggaran" => $kodePelanggaran,
				"jenis_pelanggaran" => $jenis,
				"tgl_post" => $tgl,
				"lat_long" => $langlot
			];
			$query = $this->Pelanggaran_m->simpan($data);
			if($query){
				echo "SUKSESS SIMPAN PELANGGARAN";
			}else{
				echo "GAGAL SIMPAN PELANGGARAN";
			}
		}else{
			echo "ANDA SUDAH MEMILIKI PELANGGARAN DI INFRA TERSEBUT";
		}

	}
}
//"jenis=" + jenis_pelanggaran + "&langlot=" + latlong + "&kode=" + kode + "&tgl=" + tgl
