<?php
/**
 * Created by PhpStorm.
 * User: undefined-PC
 * Date: 23/12/2018
 * Time: 15:20
 */
date_default_timezone_set('Asia/Jakarta');
class Pengguna extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->is_logged_in();
		$this->load->model('Layout_client_m');
		$this->load->model('Kendaraan_m');
		$this->load->model('Pengguna_m');
		$this->load->model('Pelanggaran_m');
	}

	public function is_logged_in(){
		if(!$this->session->userdata('is_logged') && $this->session->userdata('credentials') != "pengguna"){
			echo "<script>alert('Maaf Akses anda tidak kami ketahui!');window.location='".site_url()."/welcome'</script>";
		}
	}

	public function index(){
		$data['meta'] = $this->Layout_client_m->meta();
		$data['sidebar'] = $this->Layout_client_m->sidebar();
		$data['script'] = $this->Layout_client_m->script();

		$this->load->view('home', $data);
	}

	public function pelanggaran(){
		$data['meta'] = $this->Layout_client_m->meta();
		$data['sidebar'] = $this->Layout_client_m->sidebar();
		$data['script'] = $this->Layout_client_m->script();
		$data['pelanggaran'] = $this->Pelanggaran_m->getWhere($this->session->userdata("id_pengguna"));

		$this->load->view('pelanggaran', $data);
	}

	public function kendaraan(){
		$data['meta'] = $this->Layout_client_m->meta();
		$data['sidebar'] = $this->Layout_client_m->sidebar();
		$data['script'] = $this->Layout_client_m->script();
		$data['kendaraan'] = $this->Kendaraan_m->getAll();

		$this->load->view('kendaraan', $data);
	}

	public function do_kendaraan(){
		$jenis = $this->input->post('jenis');
		$merk = $this->input->post('merk');
		$plat = $this->input->post('plat');
		$id_pengguna = $this->session->userdata('id_pengguna');
		$tgl = date("Y-m-d H:i:s");
		$status = "on";
		$kode = "FSLKM-".uniqid($id_pengguna);

		$data = [
			"id_pengguna" => $id_pengguna,
			"kode_kendaraan" => $kode,
			"jenis_kendaraan" => $jenis,
			"merk_kendaraan" => $merk,
			"plat_nomor" => $plat,
			"status" => $status,
			"tgl_update" => $tgl
		];

		$sql = $this->Kendaraan_m->simpan($data);
		if($sql){
			echo json_encode(array("status" => 1));
		}else{
			echo json_encode(array("status" => 0));
		}
	}

	public function delete_kendaraan($id){
		if($this->Kendaraan_m->moveTrash($id)){
			echo json_encode(array("status" => 1));
		}else{
			echo json_encode(array("status" => 0));
		}
	}

	public function detail_kendaraan($id){
		$data = $this->Kendaraan_m->getWhere($id);
		echo json_encode($data);
	}

	public function update_kendaraan(){
		$id = $this->input->post("id");
		$jenis = $this->input->post('jenis');
		$merk = $this->input->post('merk');
		$plat = $this->input->post('plat');
		$id_pengguna = $this->session->userdata('id_pengguna');
		$tgl = date("Y-m-d H:i:s");
		$status = "on";
		$kode = "FSLKM-".uniqid($id_pengguna);

		$data = [
			"id_pengguna" => $id_pengguna,
			"kode_kendaraan" => $kode,
			"jenis_kendaraan" => $jenis,
			"merk_kendaraan" => $merk,
			"plat_nomor" => $plat,
			"status" => $status,
			"tgl_update" => $tgl
		];

		$sql = $this->Kendaraan_m->update($data, $id);
		if($sql){
			echo json_encode(array("status" => 1));
		}else{
			echo json_encode(array("status" => 0));
		}
	}

	public function profile(){
		$data['meta'] = $this->Layout_client_m->meta();
		$data['sidebar'] = $this->Layout_client_m->sidebar();
		$data['script'] = $this->Layout_client_m->script();
		$data['kendaraan'] = $this->Kendaraan_m->getAll();
		$data['pengguna'] = $this->Pengguna_m->getWhere($this->session->userdata('id_pengguna'));

		$this->load->view('profile', $data);
	}

	public function update_pengguna(){
		$cPass = $this->input->post('c_password');
		$cFoto = $this->input->post('c_foto');

		if($cFoto == "on"){
			$temp = $this->Pengguna_m->getWhere($this->session->userdata('id_pengguna'));
			unlink("./resource/pengguna/".$temp->file_foto);
			$config['upload_path'] = './resource/pengguna/';
			$config['allowed_types'] = 'jpg|png|svg|ico';
			$config['max_size'] = '2048';
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('file_foto')) {
				$error = array('error' => $this->upload->display_errors());
				print_r($error);
			} else {
				$data = $this->upload->data('file_name');
				if($cPass == "on"){
					$query = [
						'nik' => $this->input->post('nik'),
						'nama' => $this->input->post('nama'),
						'alamat' => $this->input->post('alamat'),
						'jenis_kelamin' => $this->input->post('jk'),
						'email' => $this->input->post('email'),
						'no_telp' => $this->input->post('telp'),
						'password' => md5($this->input->post('pass')),
						'file_foto' => $data,
						'tgl_update' => date('Y-m-d H:i:s')
					];
				}else{
					$query = [
						'nik' => $this->input->post('nik'),
						'nama' => $this->input->post('nama'),
						'alamat' => $this->input->post('alamat'),
						'jenis_kelamin' => $this->input->post('jk'),
						'email' => $this->input->post('email'),
						'no_telp' => $this->input->post('telp'),
						'file_foto' => $data,
						'tgl_update' => date('Y-m-d H:i:s')
					];
				}
				if ($this->Pengguna_m->update($query, $this->session->userdata("id_pengguna")) == TRUE) {
					echo "<script>alert('Anda berhasil melakukan perubahan data');window.location='".site_url()."/pengguna/profile';</script>";
				} else {
					echo "<script>alert('Maaf perubahan anda belum bisa terproses');window.location='".site_url()."/pengguna/profile';</script>";
				}
			}
		}else{
			if($cPass == "on"){
				$query = [
					'nik' => $this->input->post('nik'),
					'nama' => $this->input->post('nama'),
					'alamat' => $this->input->post('alamat'),
					'jenis_kelamin' => $this->input->post('jk'),
					'email' => $this->input->post('email'),
					'no_telp' => $this->input->post('telp'),
					'password' => md5($this->input->post('pass')),
					'tgl_update' => date('Y-m-d H:i:s')
				];
			}else{
				$query = [
					'nik' => $this->input->post('nik'),
					'nama' => $this->input->post('nama'),
					'alamat' => $this->input->post('alamat'),
					'jenis_kelamin' => $this->input->post('jk'),
					'email' => $this->input->post('email'),
					'no_telp' => $this->input->post('telp'),
					'tgl_update' => date('Y-m-d H:i:s')
				];
			}
			if ($this->Pengguna_m->update($query, $this->session->userdata("id_pengguna")) == TRUE) {
				echo "<script>alert('Anda berhasil melakukan perubahan data');window.location='".site_url()."/pengguna/profile';</script>";
			} else {
				echo "<script>alert('Maaf perubahan anda belum bisa terproses');window.location='".site_url()."/pengguna/profile';</script>";
			}
		}

		if($cPass != "on" && $cFoto != "on"){
			$query = [
				'nik' => $this->input->post('nik'),
				'nama' => $this->input->post('nama'),
				'alamat' => $this->input->post('alamat'),
				'jenis_kelamin' => $this->input->post('jk'),
				'email' => $this->input->post('email'),
				'no_telp' => $this->input->post('telp'),
				'tgl_update' => date('Y-m-d H:i:s')
			];
			if ($this->Pengguna_m->update($query, $this->session->userdata("id_pengguna")) == TRUE) {
				echo "<script>alert('Anda berhasil melakukan perubahan data');window.location='".site_url()."/pengguna/profile';</script>";
			} else {
				echo "<script>alert('Maaf perubahan anda belum bisa terproses');window.location='".site_url()."/pengguna/profile';</script>";
			}
		}
	}
}
