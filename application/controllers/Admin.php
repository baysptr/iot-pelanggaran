<?php
/**
 * Created by PhpStorm.
 * User: undefined-PC
 * Date: 23/12/2018
 * Time: 15:36
 */

class Admin extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->is_logged_in();
		$this->load->model('Layout_admin_m');
		$this->load->model('Pengguna_m');
	}

	public function is_logged_in(){
		if(!$this->session->userdata('is_logged') && $this->session->userdata('credentials') != "admin"){
			echo "<script>alert('Maaf Akses anda tidak kami ketahui!');window.location='".site_url()."/welcome'</script>";
		}
	}

	public function index(){
		$data['meta'] = $this->Layout_admin_m->meta();
		$data['sidebar'] = $this->Layout_admin_m->sidebar();
		$data['script'] = $this->Layout_admin_m->script();

		$this->load->view('admin/home', $data);
	}

	public function pengguna(){
		$data['meta'] = $this->Layout_admin_m->meta();
		$data['sidebar'] = $this->Layout_admin_m->sidebar();
		$data['script'] = $this->Layout_admin_m->script();
		$data['pengguna'] = $this->Pengguna_m->getAll();

		$this->load->view('admin/pengguna', $data);
	}
}
