<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Pengguna_m");
	}

	public function index()
	{
		$this->login();
	}

	public function daftar(){
		$this->load->view('daftar');
	}

	public function login(){
		$this->load->view('login');
	}

	public function do_login(){
		$data = [
			'email' => $this->input->post('email'),
			'password' => md5($this->input->post('password')),
		];

		$check = $this->Pengguna_m->check_login($data);

		if($check->num_rows() > 0){
			$data = $check->row();
			if($data->status == "on"){
				$data_session = [
					"id_pengguna" => $data->id,
					"credentials" => $data->credentials,
					"status" => $data->status,
					"is_logged" => "in"
				];
				$this->session->set_userdata($data_session);
				if($data->credentials == "admin"){
					redirect(site_url().'/admin');
				}else{
					redirect(site_url().'/pengguna');
				}
			}else{
				echo "<script>alert('Maaf akun anda belum terverifikasi, silahkan hubungi admin');window.location='".site_url()."/welcome/login'</script>";
			}
		}else{
			echo "<script>alert('Maaf akun anda tidak kami ketahui');window.location='".site_url()."/welcome/login'</script>";
		}
	}

	public function do_logout(){
		$this->session->sess_destroy();
		redirect(site_url().'/admin');
	}

	public function do_daftar(){
		$config['upload_path'] = './resource/pengguna/';
		$config['allowed_types'] = 'jpg|png|svg|ico';
		$config['max_size'] = '2048';
		$config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('file_foto')) {
			$error = array('error' => $this->upload->display_errors());
			print_r($error);
		} else {
			$data = $this->upload->data('file_name');
			$query = [
				'nik' => $this->input->post('nik'),
				'nama' => $this->input->post('nama'),
				'alamat' => $this->input->post('alamat'),
				'jenis_kelamin' => $this->input->post('jk'),
				'email' => $this->input->post('email'),
				'no_telp' => $this->input->post('telp'),
				'password' => md5($this->input->post('pass')),
				'file_foto' => $data,
				'credentials' => 'pengguna',
				'status' => 'on',
				'tgl_update' => date('Y-m-d H:i:s')
			];
			if ($this->Pengguna_m->simpan($query) == TRUE) {
				echo "<script>alert('Anda berhasil mendaftar sebagai pengguna, silahkan konfirmasi ke admin untuk aktivasi akun pengguna');window.location='".site_url()."/welcome/login';</script>";
			} else {
				echo "<script>alert('Maaf data anda belum bisa diproses, mohon periksa kembali');window.location='".site_url()."/welcome/daftar';</script>";
			}
		}
	}
}
