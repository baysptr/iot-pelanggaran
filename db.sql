-- MySQL dump 10.16  Distrib 10.1.30-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: iot_pelanggaran
-- ------------------------------------------------------
-- Server version	10.1.30-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `kendaraan`
--

DROP TABLE IF EXISTS `kendaraan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kendaraan` (
  `id_kendaraan` int(4) NOT NULL AUTO_INCREMENT,
  `id_pengguna` int(4) NOT NULL,
  `kode_kendaraan` varchar(100) NOT NULL,
  `jenis_kendaraan` varchar(20) NOT NULL,
  `merk_kendaraan` varchar(20) NOT NULL,
  `plat_nomor` varchar(15) NOT NULL,
  `status` enum('on','off') NOT NULL,
  `tgl_update` datetime NOT NULL,
  PRIMARY KEY (`id_kendaraan`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kendaraan`
--

LOCK TABLES `kendaraan` WRITE;
/*!40000 ALTER TABLE `kendaraan` DISABLE KEYS */;
INSERT INTO `kendaraan` VALUES (1,4,'FSLKM-45c1f9d7bc2aff','SEPEDA MOTOR','YAMAHA','L 6125 QZ','on','2018-12-23 21:36:43'),(2,4,'FSLKM-45c1f920f3ff95','test','test','test','off','2018-12-23 20:47:59'),(3,4,'FSLKM-45c1f92c73adda','test','test','test','off','2018-12-23 20:51:03');
/*!40000 ALTER TABLE `kendaraan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pelanggaran`
--

DROP TABLE IF EXISTS `pelanggaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pelanggaran` (
  `id_pelanggaran` int(4) NOT NULL AUTO_INCREMENT,
  `id_kendaraan` int(4) NOT NULL,
  `kode_pelanggaran` varchar(100) NOT NULL,
  `jenis_pelanggaran` enum('stop','parkir') NOT NULL,
  `tgl_post` datetime NOT NULL,
  `lat_long` text NOT NULL,
  PRIMARY KEY (`id_pelanggaran`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pelanggaran`
--

LOCK TABLES `pelanggaran` WRITE;
/*!40000 ALTER TABLE `pelanggaran` DISABLE KEYS */;
INSERT INTO `pelanggaran` VALUES (4,1,'PELANGGAR-5c20b53ac7398','parkir','2018-12-24 19:25:30','-7.289951,112.775302');
/*!40000 ALTER TABLE `pelanggaran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pengguna`
--

DROP TABLE IF EXISTS `pengguna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengguna` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `nik` int(25) NOT NULL,
  `nama` varchar(55) NOT NULL,
  `jenis_kelamin` enum('Laki - laki','Perempuan') NOT NULL,
  `alamat` varchar(55) NOT NULL,
  `email` varchar(40) NOT NULL,
  `no_telp` varchar(17) NOT NULL,
  `password` varchar(225) NOT NULL,
  `file_foto` varchar(255) NOT NULL,
  `credentials` enum('admin','pengguna') NOT NULL,
  `status` enum('on','off') NOT NULL,
  `tgl_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pengguna`
--

LOCK TABLES `pengguna` WRITE;
/*!40000 ALTER TABLE `pengguna` DISABLE KEYS */;
INSERT INTO `pengguna` VALUES (1,2147483647,'Admin','Laki - laki','Jl. My Admin','admin@host.cc','011223445667','0eff44c362b13fa25fc88a412f5512e1','unkwon','admin','on','0000-00-00 00:00:00'),(4,2147483647,'Putra Bangsa','Laki - laki','Jl. Indonesia Raya No. 45','ind@nesi.a','08967727157','25d55ad283aa400af464c76d713c07ad','8b0ba63bdad57a26fabe33c2f7027771.jpg','pengguna','on','2018-12-24 14:01:18');
/*!40000 ALTER TABLE `pengguna` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-25  1:12:28
